# Chip8-emulator
A basic Chip8 emulator written in C++ using the SDL2 library

# Building
Install dependencies, dir into the clone folder and run 'make'

```
$ doas pacman -Syyu
$ doas pacman -S sdl2 sdl2_gfx sdl2_image sdl2_mixer
$ cd dir/to/clone
$ make
```
After it has compiled run:

```
$ ./out dir/to/ROM
```

# Images 

![alt text](https://raw.githubusercontent.com/Joe-Todd0/Chip8-emulator/master/image.png)

# TODO

 - FIX WEIRD SEG FAULT ON CERTAIN ROMS
