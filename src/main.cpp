#include <iostream>
#include <fstream>

#include "chip8.hpp"

Chip8 obj;

int main(int argc, char** argv)
{
	std::cout << "test: " << argc << std::endl;
	
	if(!argv[1])
	{
		std::cout << "Use: ./out dir/to/rom" << std::endl;
		return EXIT_FAILURE;
	}
	
	if(!obj.initsdl())
	{
		return EXIT_FAILURE;
	}
	
	obj.fill_buf();

	std::cout << "Initializing main funcs" << std::endl;
	obj.initemu();

	if(!obj.load_rom(argv[1]))
	{
		return EXIT_FAILURE;
	}

	while(obj.quit == false)
	{
		if(SDL_PollEvent(&obj.event))
		{
			switch(obj.event.type)
			{
				case SDL_KEYDOWN:
				case SDL_KEYUP:
					obj.key_press(obj.event);
			}
		}
		obj.cycle();	
		obj.clocktick();

		obj.render(obj.gfx);
		obj.playbeep();

		SDL_Delay(3);
	}
	obj.destroy();

	return EXIT_SUCCESS;
}
