#pragma once

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#define COLOR 2

class Chip8
{
	private:
		// MAIN EMU STUFF
		
		// 35 opcodes for processing
		unsigned short opcode;
	
		// 4k of memory
		unsigned char memory[4096];

		// opcode stack and pointer
		unsigned short stack[16];
		unsigned short sp;

		// CPU reg
		unsigned char V[16];

		// index reg and prog counter 
		unsigned short I;
		unsigned short pc;

		// WINDOW SDL2 STUFF
		SDL_Rect fill = {0,0,8,8};
		
		SDL_Window* window = nullptr;
		SDL_Surface* surface = nullptr;
		Mix_Chunk* sound = nullptr;

		u_int16_t scale = 8;

		u_int32_t p[2];

		const u_int8_t* kstate = nullptr;

		bool logs = false;
	public:
		Chip8();
		~Chip8();

		// main emu stuff
		// timers
		uint8_t delay_timer;
		unsigned char sound_timer;
		
		static const u_int16_t H = 32;
		static const u_int16_t W = 64;

		unsigned char gfx[64][32];		

		unsigned char key[16];

		void initemu();
			
		bool load_rom(std::string);

		void cycle();

		void clocktick();
			
		// sdl2 stuff
		bool quit = false;
		bool pause = false;
		bool initsdl();
		
		SDL_Event event;

		int waitkey();
		int key_press(SDL_Event event);

		void destroy();
		void render(unsigned char gfx[64][32]);
		void playbeep();
		void fill_buf();
};
