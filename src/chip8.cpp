#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <iomanip>

#include "chip8.hpp"
#define MAX_M 4096

void Chip8::initemu()
{	
	pc = 0x200; // reset prog counter
	opcode = 0x0000; // reset opcode pos
	I = 0; // reset index
	sp = 0; // reset stack pointer

	// start to reset mem
	memset(V, 0,  sizeof(V));
	memset(stack, 0, sizeof(stack));
	memset(memory, 0, sizeof(memory));
	memset(key, 0, sizeof(key));
	std::cout << "stack after memset: " << stack << std::endl;

	unsigned char chip8_fontset[80] =
	{ 
	  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	  0x20, 0x60, 0x20, 0x20, 0x70, // 1
	  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
	};
		
	// load fontset into mem
	for(int i=0;i<80;i++)
	{
		memory[i] = chip8_fontset[i];
	}
	
	delay_timer = 0;
	sound_timer = 0;
}

bool Chip8::load_rom(std::string rom)
{
	std::ifstream f(rom, std::ios::in|std::ios::binary|std::ios::ate);
	
	if(!f.is_open())
	{
		std::cout << "error opening rom file" << std::endl;
		return false;	
	}	
	auto size = f.tellg();
	
	if(size > 4096)
	{
		std::cout << "Error ROM to big" << std::endl;
		return false;	
	}
	f.seekg(0,std::ios::beg);
	f.read((char*)&memory[0x200],size);
	f.close();
	
	std::cout << "ROM loaded successfully" << std::endl;

	return true;
}

void Chip8::cycle()
{
	SDL_Delay(1);
	
	opcode = memory[pc] << 8 | memory[pc + 1];

	unsigned short address = opcode & 0x0FFF;
	unsigned char* VX = &V[(opcode & 0x0F00) >> 8]; // VX equals  to v at index opcode shifted to right 8 bits
	unsigned char* VY = &V[(opcode & 0x00F0) >> 4]; // VY same as VX but shifted 4 bits

	//if(logs)
	
	std::cout << std::hex << std::setfill('0') << std::setw(2) << "Current instruction: 0x" << opcode << std::endl;
	std::cout << std::hex << std::setfill('0') << std::setw(2) << "Current opcode: 0x" << (opcode & 0xF000) << std::endl;
	std::cout << "PC: " << this->pc << std::endl;
	//}

	std::cout << "Stack: " << this->stack << std::endl;
	std::cout << "NNN: " << address << std::endl;

	switch(opcode & 0xF000)
	{
		case 0x0000:
			switch(opcode & 0x0FFF)
			{
				case 0x00E0: //0x00E0 clears the screen
					for(int i=0;i<64;++i)
					{
						for(int j=0;j<32;++j)
						{
							gfx[i][j]=0;
						}
					}	
					pc += 2;
					
					break;

				case 0x00EE: // 0x00EE returns from subroutine
					// when we return from a subroutine we need to set the stack pointer
					// back one as that is where we saved the latest address before 
					// the jump
					sp--;
					pc=stack[sp];
					pc += 2;

					break;

				default:
					std::cout << "Unknown opcode 0x%X\n" + opcode << std::endl;

					break;
			}
		break;

		case 0xA000: // ANNN sets I (index reg) to the adress NNN
			// exec		
			I = address;
			pc += 2;
			
			break;
		
		case 0x1000: // 1NNN jump to address NNN
			pc = address;

			break;

		case 0x2000: // calls sub at NNN
			// here we need to jump to another adress for the time being 
			// so we need to save the current address before hand.
			stack[sp] = pc;
			sp++;
			pc = address;

			break;

		case 0x3000: // skips next instruction if VX is equal to NN (0x00FF)
			if(*VX==(opcode & 0x00FF))
			{
				pc += 4;
			} else {
				pc += 2;
			}

			break;
		
		case 0x4000: // skips next instruction if VX does NOT equal NN
			if(*VX!=(opcode & 0x00FF))
			{
				pc += 4;
			} else {
				pc += 2;
			}

			break;

		case 0x5000: // skips next instruction if VX equals to VY
			if(*VX==*VY)
			{
				pc += 4;
			} else {
				pc += 2;
			}
			
			break;

		case 0x6000: // sets VX to NN
			*VX = (opcode & 0x00FF);
			pc += 2;

			break;
			
		case 0x7000: // adds NN to VX (carry flag is not changed)
			*VX += (opcode & 0x00FF);
			pc += 2;

			break;

		case 0x8000: 
			// here we need to use another switch as we have many potential 0x8000s
			switch(opcode & 0x000F)
			{
				case 0x0000: // sets VX to the value of VY
					*VX = *VY;
					pc += 2;

					break;
				
				case 0x0001: // sets VX to VX or VY (bitwise)
					*VX = *VX | *VY;
					pc += 2;

					break;
				
				case 0x0002: // sets VX to VX and VY (bitwise AND)
					*VX = *VX & *VY;
					pc += 2;

					break;

				case 0x0003: // sets VX to VX xor VY (xor gate)
					// we use ^ for XOR opperations
					*VX = *VX ^ *VY;
					pc += 2;

					break;

				case 0x0004: // adds VY to VX. VF is set to 1 when there's a carry, and 0
					     // when there isn't.
					// here we use "0xF" to simply leave the last 8 bits of VX and ignore 
					// the rest.

					if((0xFF - *VX) < *VY)
					{
						V[0xF] = 1; // carry
					} else {
						V[0xF] = 0; // don't carry
					}
					*VY += *VX;
					pc += 2;

					break;

				case 0x0005: // VY is taken away from VX. VF is set to 0 when there's a borrow,
				       	     // and 1 when there isn't.
					if(V[(opcode & 0x0F0) >> 4] <= *VX)
					{
						V[0xF] = 1; // then we borrow
					} else {
						V[0xF] = 0; // no need to borrow
					}
					*VX = *VX - *VY;
					pc += 2;

					break;
		
				case 0x0006: // stores the least signif bit of VX in VF and then shifts VX to
					     // the right by 1. We assume that 0x0001 is the least signif.
					V[0xF] = *VX & 0x0001;				
					*VX = *VX >> 1;
				
					pc += 2;

					break;

				case 0x0007: // sets VX to VY minus VX. VF is set to 0 when there's a borrow
					     // and 1 when there isn't
					if(*VX <= *VY)
					{
						V[0xF] = 1; // carry
					} else {
						V[0xF] = 0; // 
					}
					*VX = *VY - *VX;
					pc += 2;

					break;
				
				case 0x000E: // stores the most signif bit of VX in VF and then shifts VX to the
					     // the left by 1
					V[0xF] = static_cast<uint8_t>((V[*VY] & 0x80) >> 7); // shift right 7 bits
					*VX = *VX << 1; // shift to left 1 bit
					
					pc += 2;
					break;
					
				default:
					std::cout << "Unknown opcode 0x%X\n" + opcode << std::endl;
					break;
							
			}
			break;

		case 0x9000: // skips to next instruction if VX doesn't equal VY
			if(*VX != *VY)
			{
				pc += 4;
			} else {
				pc += 2;
			}  

			break;

		case 0xB000: // jumps to address NNN plus V0
			pc = V[0x0] + (opcode & 0x0FFF);
			break;

		case 0xC000: // sets VX to the result of a bitwise AND operation
			     // on a random number and NN.
			*VX = rand() & (address);
			pc += 2;

			break;
		
		case 0xD000: // Draws a sprite at cords (VX,VY) that has a width of 8p and a height of N+1p.
			// Help from other emulators online and stack overflow
			// as was unable to add this one myself :)
			{
				int height = opcode & 0x000F;
				V[0xF] = 0;

				for(int y=0; y<height; y++)
				{
					int pixels = memory[I+y];

					for(int x=0; x<8; ++x)
					{
						if((pixels & (0x80 >> x)) !=0)
						{
							if(gfx[*VX + x][*VY + y] == 1)
							{
								V[0xF] = 1;
							}
							gfx[*VX + x][*VY + y] ^= 1;
						}
					}	
				}
				pc += 2;
			}
			break;
	 
		case 0xE000:
			switch(opcode & 0x00FF)
			{
				case 0x009E: // skips next instruction if the key stored in VX is pressed.
					if(key[*VX] == 1)
					{
						pc += 4;
					} else {
						pc += 2;	
					}			

					break;

				case 0x00A1: // skips next instruction if the key stored in VX isn't pressed	
					if(key[*VX] == 0)
					{
						pc += 4;
					} else {
						pc += 2;
					}
											
					break;
				
				default:
					std::cout << "Unknown opcode 0x%X\n" + opcode << std::endl;
					
			}
		
			break;

		case 0xF000:
		{
			switch(opcode & 0x00FF)
			{
				case 0x0007: // sets VX to the value of the delay timer
					*VX = delay_timer;
					pc += 2;

					break;

				case 0x000A: // a key press is awaited and then stored in VX
					// we cycle through ALL of the key buffer and see if one of them
					// is equal to 1 aka it has been pressed.
					*VX = waitkey();
					pc += 2;
					
					break;
			
				case 0x0015: // sets the delay timer to VX
					delay_timer = *VX;
					pc += 2;

					break;

				case 0x0018: // sets the sound timer to VX
					sound_timer = *VX;
					pc += 2;

					break;

				case 0x001E: // adds VX to I
					if(I + *VX > 0xFFF)
					{
						V[0xF] = 1;
					} else {
						V[0xF] = 0;
					}		
					I += *VX;
					pc += 2;

					break;

				case 0x0029: // set I to the location of the sprite for the character in VX
					I = *VX * 5;
					pc += 2;

					break;

				case 0x0033: // stores the binary-coded decimal representation of VX
					memory[I] = *VX / 100;
					memory[I + 1] = *VX % 100 / 10; 
					memory[I + 2] = *VX % 10;

					pc += 2;
					break;

				case 0x0055: // stores V0 to VX in memory starting at address I
					for(int j=0;j<((opcode & 0x0F00) >> 8) + 1;++j)
					{
						memory[I + j] = V[j];
					}
					pc += 2;

					break;

				case 0x0065: // fills V0 to VX with the values from memory starting at address
					     // I.
					for(int j=0;j<((opcode & 0x0F00) >> 8) + 1;++j)
					{
						V[j] = memory[I + j];
					}
					pc += 2;

					break;
				default:
					std::cout << "Unknown opcode 0x%X\n" + opcode << std::endl;

			}
			break;
		}
	}
}

void Chip8::clocktick()
{
	while(delay_timer > 0)
	{
		--delay_timer;
		SDL_Delay(1);
	}
}

// constructor
Chip8::Chip8()
{
// nothing 
}

// deconstructor
Chip8::~Chip8()
{
// nothing 
}
