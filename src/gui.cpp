#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "chip8.hpp"

bool Chip8::initsdl()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << SDL_GetError() << std::endl;
		return false;
	}
	
	window = SDL_CreateWindow("Chip8 Emulator", SDL_WINDOWPOS_CENTERED
			,SDL_WINDOWPOS_CENTERED, W * scale, H *scale, 0);

	if(!window)
	{
		std::cout << SDL_GetError() << std::endl;
		return false;
	}
	surface = SDL_GetWindowSurface(window);

	Mix_OpenAudio(22050,AUDIO_S16SYS,2,640);

	sound = Mix_LoadWAV("beep.ogg");
	if(sound == nullptr)
	{
		std::cout << SDL_GetError() << std::endl;
		return false;
	}
	

	return true;
}

int Chip8::key_press(SDL_Event event)
{	
	// so, here we are going to check the event and check every valid key to see if one is true, if so
	// we set the corrosponding place in the array 'key' to kcode. (not the best explanation i know)
	int kcode;
	
	// event.key.keysym.sym
	switch(event.key.keysym.sym)
	{
		case SDLK_x:
			kcode = 0x0;

			break;
		case SDLK_0:
			// for some reason you have to hold 0 for a few seconds for it to stay enabled
			// do not know why, but it does
			// it is a *feature* 
			if(logs)
			{
				logs = false;
			} else {
				logs = true;
			}
			break;
		case SDLK_TAB:
			while(SDL_WaitEvent(&event) and !pause)
			{
				switch(event.type)
				{
					case SDL_KEYUP:
					case SDL_KEYDOWN:
						pause = true;
						break;
				}
			}
			break;
		
		case SDLK_1:
			kcode = 0x1;

			break;
		case SDLK_2:
			kcode = 0x2;

			break;
		case SDLK_3:
			kcode = 0x3;

			break;
		case SDLK_q:
			kcode = 0x4;

			break;
		case SDLK_w:
			kcode = 0x5;

			break;
		case SDLK_e:
			kcode = 0x6;

			break;
		case SDLK_a:
			kcode = 0x7;

			break;
		case SDLK_s:
			kcode = 0x8;

			break;
		case SDLK_d:
			kcode = 0x9;

			break;
		case SDLK_z:
			kcode = 0xA;

			break;
		case SDLK_c:
			kcode = 0xB;

			break;
		case SDLK_4:
			kcode = 0xC;

			break;
		case SDLK_r:
			kcode = 0xD;

			break;
		case SDLK_f:
			kcode = 0xE;

			break;
		case SDLK_v:
			kcode = 0xF;

			break;
		case SDLK_ESCAPE:
			destroy();
			break;
		default:
			return -1;
	}	
	// we grab key from inside the class
	key[kcode] = (event.type == SDL_KEYDOWN); 

	return kcode;
}

int Chip8::waitkey()
{
	std::cout << "PRESS A KEY" << std::endl;

	while(SDL_WaitEvent(&event))
	{
		switch(event.type)
		{
			case SDL_KEYUP:
			case SDL_KEYDOWN:
				int i=key_press(event);
				
				if(i != -1)
				{					
					return i;
				}
			break;
		}
	}
	return EXIT_SUCCESS;
}

void Chip8::destroy()
{
	// we need to keep this function as SDL does not have some sort of close function
	// .When we want to exit the emulator we need to call this function.
	std::cout << "\aDestroying SDL" << std::endl;

	Mix_FreeChunk(sound);
	Mix_CloseAudio();

	SDL_DestroyWindow(window);
	SDL_Quit();
	std::exit(1);
}

void Chip8::fill_buf()
{
	// couldnt figure this out so i asked for a little help :)
	p[0] = SDL_MapRGB(surface->format, 0x00, 0x00, 0x00);
	p[1] = SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF);
}

void Chip8::render(unsigned char gfx[W][H])
{
	for(int i=0;i<W;++i)
	{
		fill.x = i*8;
		
		for(int j=0;j<H;++j)
		{
			fill.y = j*8;
			SDL_FillRect(surface, &fill, p[gfx[i][j]]);
		}
	}
	SDL_UpdateWindowSurface(window);
}

void Chip8::playbeep()
{
	if(sound_timer > 0)
	{
		if(Mix_PlayingMusic() == 0)
		{
			Mix_PlayChannel(-1,sound,0);	
		} else {
			Mix_HaltMusic();
		}
	}
}

